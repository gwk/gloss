# Gloss shell utilities.


# VSCode Development

* Press `F5` to launch a new instance of the editor with the extension loaded.
* If it does not work the first time then you may need to go to the debug mode (click the bug icon in the left toolbar of VSCode), and launch it with the GUI controls at the top of the pane.
* In the debug instance, select `File > Preferences > Color Themes` and pick `gloss-black`.
* Changes can be reloaded with `Cmd+R` from the debug instance.

